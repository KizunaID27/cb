<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Cache;
use App\Http\Controllers\Apis\SessionID;
use DB;

class BasicTest extends TestCase
{   

    private function generateToken()
    {
        $mutations_id = DB::table('mutations')->where('id',1)->first();

        $new = new SessionID();        
        return $new->generate($mutations_id);
    }
    
    public function testLogin()
    {
        $response = $this->json('POST', '/api/login',[
                'email'=>'ferry@crocodic.com',
                'password'=>'123456'
            ])
            ->assertStatus(200)
            ->assertJson([
                'api_status' => 1                
            ]);
    }

    public function testHitDefault()
    {
        $this->json('POST','/api/default',[], ['sid'=>$this->generateToken()])
        ->assertStatus(200)
        ->assertJson(['api_status'=>1]);
    }

    public function testHitHome()
    {
        $this->json('POST','/api/home',[], ['sid'=>$this->generateToken()])
        ->assertStatus(200)
        ->assertJson(['api_status'=>1]);   
    }

    public function testHitForgot()
    {
        // $this->json('POST','/api/forgot_password',['email'=>'ferry@crocodic.com'])
        // ->assertStatus(200)
        // ->assertJson(['api_status'=>1]);      
    }
}
