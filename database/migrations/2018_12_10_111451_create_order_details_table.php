<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('orders_id')->nullable();
			$table->integer('products_id')->nullable();
			$table->string('products_code')->nullable();
			$table->string('products_name')->nullable();
			$table->float('products_price', 10, 0)->nullable();
			$table->float('disc_precentage', 10, 0)->nullable();
			$table->float('disc_price', 10, 0)->nullable();
			$table->integer('quantity')->nullable();
			$table->float('total', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_details');
	}

}
