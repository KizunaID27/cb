<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourierTaskOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courier_task_orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('courier_tasks_id')->nullable();
			$table->integer('orders_id')->nullable();
			$table->dateTime('reported_at')->nullable();
			$table->string('reported_description', 300)->nullable();
			$table->string('customer_signature')->nullable();
			$table->unique(['courier_tasks_id','orders_id'], 'courier_tasks_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courier_task_orders');
	}

}
