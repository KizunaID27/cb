<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupplierSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supplier_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('label')->nullable();
			$table->string('name')->nullable();
			$table->text('content', 65535)->nullable();
			$table->integer('suppliers_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supplier_settings');
	}

}
