<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWfFeatureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wf_feature', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 30);
			$table->string('content', 200);
			$table->text('icon', 65535);
			$table->integer('order_number');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wf_feature');
	}

}
