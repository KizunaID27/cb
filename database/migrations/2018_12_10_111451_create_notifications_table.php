<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('recipient_mutations_id')->nullable();
			$table->string('title')->nullable();
			$table->string('content', 500)->nullable();
			$table->string('id_object')->nullable();
			$table->string('detail_action')->nullable();
			$table->boolean('is_read')->nullable()->default(0);
			$table->string('recipient_tokens')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
