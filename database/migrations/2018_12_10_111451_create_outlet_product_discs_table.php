<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOutletProductDiscsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('outlet_product_discs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('products_id')->nullable();
			$table->integer('supplier_outlets_id')->nullable();
			$table->float('precent_disc', 10, 0)->nullable();
			$table->float('amount_disc', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('outlet_product_discs');
	}

}
