<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesPlanCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_plan_categories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('sales_plans_id')->nullable();
			$table->integer('product_categories_id')->nullable();
			$table->string('product_categories_name')->nullable();
			$table->unique(['sales_plans_id','product_categories_id'], 'sales_plans_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_plan_categories');
	}

}
