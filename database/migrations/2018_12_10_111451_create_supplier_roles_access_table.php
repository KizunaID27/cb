<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupplierRolesAccessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supplier_roles_access', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('supplier_roles_id')->nullable();
			$table->integer('cms_menus_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supplier_roles_access');
	}

}
