<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOutletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('outlets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('cms_users_id')->nullable();
			$table->string('outlet_name')->nullable();
			$table->integer('outlet_types_id')->nullable();
			$table->string('outlet_type')->nullable()->default('Grosir');
			$table->string('photo')->nullable();
			$table->string('owner_name')->nullable();
			$table->string('owner_address')->nullable();
			$table->string('owner_ktp')->nullable();
			$table->string('owner_npwp')->nullable();
			$table->string('outlet_address')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->string('outlet_phone')->nullable();
			$table->string('owner_phone')->nullable();
			$table->time('open_hour')->nullable();
			$table->time('close_hour')->nullable();
			$table->integer('regions_id')->nullable();
			$table->integer('areas_id')->nullable();
			$table->integer('territories_id')->nullable();
			$table->integer('provinces_id')->nullable();
			$table->integer('cities_id')->nullable();
			$table->integer('sub_districts_id')->nullable();
			$table->integer('is_approved')->nullable();
			$table->boolean('is_ppn')->nullable();
			$table->integer('registered_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('outlets');
	}

}
