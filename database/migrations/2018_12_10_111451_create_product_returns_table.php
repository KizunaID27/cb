<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductReturnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_returns', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('created_at')->nullable();
			$table->integer('sales_mutations_id')->nullable();
			$table->integer('visit_plan_outlet_dates_id')->nullable();
			$table->integer('courier_task_orders_id')->nullable();
			$table->integer('outlets_id')->nullable();
			$table->integer('products_id')->nullable();
			$table->integer('quantity')->nullable();
			$table->integer('source_order_details_id')->nullable();
			$table->integer('source_orders_id')->nullable();
			$table->integer('target_orders_id')->nullable();
			$table->float('price_total', 10, 0)->nullable();
			$table->boolean('is_approved')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_returns');
	}

}
