<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitPlanBillingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visit_plan_billings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('visit_plan_outlet_dates_id')->nullable();
			$table->integer('orders_id')->nullable();
			$table->float('paid_amount', 10, 0)->nullable();
			$table->string('signature')->nullable();
			$table->unique(['visit_plan_outlet_dates_id','orders_id'], 'visit_plan_outlet_dates_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visit_plan_billings');
	}

}
