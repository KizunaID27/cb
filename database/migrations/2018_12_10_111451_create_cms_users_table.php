<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('suppliers_id')->nullable();
			$table->integer('id_cms_privileges')->nullable();
			$table->integer('mutations_id')->nullable();
			$table->date('start_work_at')->nullable();
			$table->string('name')->nullable();
			$table->string('photo')->nullable();
			$table->string('idcard_number', 55)->nullable();
			$table->string('gender', 55)->nullable()->default('Laki-laki');
			$table->string('phone')->nullable();
			$table->string('address')->nullable();
			$table->string('username')->nullable();
			$table->string('email')->nullable();
			$table->string('password')->nullable();
			$table->string('status', 50)->nullable()->default('Active');
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->dateTime('ping_at')->nullable();
			$table->string('app_version', 50)->nullable();
			$table->string('regid')->nullable();
			$table->integer('supplier_roles_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_users');
	}

}
