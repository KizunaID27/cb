<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_plans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_year')->nullable();
			$table->integer('plan_month')->nullable();
			$table->integer('mutations_id')->nullable();
			$table->unique(['plan_year','plan_month','mutations_id'], 'plan_year_plan_month_mutations_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_plans');
	}

}
