<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('supplier_business_id')->nullable();
			$table->string('code')->nullable();
			$table->string('photo')->nullable();
			$table->string('company_name')->nullable();
			$table->string('company_address')->nullable();
			$table->string('company_phone')->nullable();
			$table->string('owner_name')->nullable();
			$table->string('owner_address')->nullable();
			$table->string('owner_ktp')->nullable();
			$table->string('owner_npwp')->nullable();
			$table->string('owner_phone', 55)->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->string('status', 55)->nullable()->default('Pending');
			$table->integer('is_approved')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suppliers');
	}

}
