<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupplierOutletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supplier_outlets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('suppliers_id')->nullable();
			$table->integer('mutations_id')->nullable();
			$table->integer('registered_by_mutation')->nullable();
			$table->integer('outlets_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supplier_outlets');
	}

}
