<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesPlanCatProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_plan_cat_products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('sales_plan_categories_id')->nullable();
			$table->integer('products_id')->nullable();
			$table->string('products_code')->nullable();
			$table->string('products_name')->nullable();
			$table->float('products_price', 10, 0)->nullable();
			$table->float('plan_unit', 10, 0)->nullable();
			$table->float('plan_value', 10, 0)->nullable();
			$table->float('real_unit', 10, 0)->nullable();
			$table->float('real_value', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_plan_cat_products');
	}

}
