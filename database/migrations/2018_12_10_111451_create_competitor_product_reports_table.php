<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitorProductReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competitor_product_reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('visit_plan_outlet_dates_id')->nullable();
			$table->string('photo')->nullable();
			$table->string('name')->nullable();
			$table->float('price', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competitor_product_reports');
	}

}
