<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitPlanOutletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visit_plan_outlets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('visit_plans_id')->nullable();
			$table->integer('outlets_id')->nullable();
			$table->boolean('is_incident')->nullable();
			$table->unique(['visit_plans_id','outlets_id'], 'visit_plans_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visit_plan_outlets');
	}

}
