<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('visit_plan_outlet_dates_id')->nullable();
			$table->string('order_no')->nullable();
			$table->integer('suppliers_id')->nullable();
			$table->integer('sales_id')->nullable()->comment('mutation of sales');
			$table->string('sales_name')->nullable();
			$table->integer('mutations_id')->nullable();
			$table->string('mutations_code')->nullable();
			$table->integer('outlets_id')->nullable();
			$table->string('outlets_name')->nullable();
			$table->string('destination_address')->nullable();
			$table->string('destination_latitude')->nullable();
			$table->string('destination_longitude')->nullable();
			$table->date('order_date')->nullable();
			$table->date('payment_expired_at')->nullable();
			$table->string('payment_method')->nullable();
			$table->float('sub_total', 10, 0)->nullable();
			$table->float('ppn', 10, 0)->nullable();
			$table->float('total', 10, 0)->nullable();
			$table->string('status', 55)->nullable()->default('Order Baru');
			$table->string('status_description')->nullable();
			$table->boolean('is_lunas')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
