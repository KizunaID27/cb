<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitPlanOutletDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visit_plan_outlet_dates', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('visit_plan_outlets_id')->nullable();
			$table->integer('date_no')->nullable();
			$table->date('plan_date')->nullable();
			$table->boolean('is_visited')->nullable();
			$table->dateTime('visited_at')->nullable();
			$table->string('visited_signature')->nullable();
			$table->string('return_deduct_type', 55)->nullable();
			$table->unique(['visit_plan_outlets_id','date_no'], 'visit_plan_outlets_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visit_plan_outlet_dates');
	}

}
