<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('suppliers_id')->nullable();
			$table->integer('product_categories_id')->nullable();
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('packaging')->nullable();
			$table->string('image')->nullable();
			$table->float('price', 10, 0)->nullable();
			$table->float('max_percent_disc', 10, 0)->nullable();
			$table->string('description')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
