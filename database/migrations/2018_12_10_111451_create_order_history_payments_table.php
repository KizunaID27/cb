<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderHistoryPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_history_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('orders_id')->nullable();
			$table->integer('courier_task_orders_id')->nullable();
			$table->integer('sales_mutations_id')->nullable();
			$table->float('amount', 10, 0)->nullable();
			$table->string('signature_pic')->nullable();
			$table->boolean('is_paid_to_admin')->nullable();
			$table->integer('product_returns_id')->nullable();
			$table->string('remark')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_history_payments');
	}

}
