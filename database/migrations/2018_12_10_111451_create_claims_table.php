<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClaimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claims', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('created_at')->nullable();
			$table->date('claim_date')->nullable();
			$table->integer('claim_types_id')->nullable();
			$table->float('amount', 10, 0)->nullable();
			$table->integer('mutations_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claims');
	}

}
