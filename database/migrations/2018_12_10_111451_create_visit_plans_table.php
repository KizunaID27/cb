<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visit_plans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code')->nullable();
			$table->integer('mutations_id')->nullable();
			$table->integer('plan_year')->nullable();
			$table->integer('plan_month')->nullable();
			$table->integer('target_new_outlet')->nullable();
			$table->integer('target_billing')->nullable();
			$table->integer('target_visit_selling')->nullable();
			$table->unique(['mutations_id','plan_year','plan_month'], 'mutations_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visit_plans');
	}

}
