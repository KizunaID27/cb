<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCashTransferHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cash_transfer_histories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('created_at')->nullable();
			$table->integer('mutations_id')->nullable();
			$table->string('admin_name')->nullable();
			$table->float('amount', 10, 0)->nullable();
			$table->string('transfer_to', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cash_transfer_histories');
	}

}
