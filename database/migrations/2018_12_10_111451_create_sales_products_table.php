<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('mutations_id')->nullable();
			$table->integer('products_id')->nullable();
			$table->unique(['mutations_id','products_id'], 'cms_users_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_products');
	}

}
