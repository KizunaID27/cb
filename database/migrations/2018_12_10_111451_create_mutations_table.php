<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMutationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mutations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('created_at')->nullable();
			$table->string('code', 55)->nullable();
			$table->integer('cms_users_id')->nullable();
			$table->integer('cms_privileges_id')->nullable();
			$table->integer('regions_id')->nullable();
			$table->integer('areas_id')->nullable();
			$table->integer('territories_id')->nullable();
			$table->integer('parent_mutations_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mutations');
	}

}
