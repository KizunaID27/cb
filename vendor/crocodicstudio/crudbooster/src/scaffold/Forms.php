<?php

namespace crocodicstudio\crudbooster\scaffold;

use crocodicstudio\crudbooster\scaffold\types\TextModel;

class Forms
{
    private static $cols = [];

    private static function putSingleton() {
        app('crudbooster_columns')->addForm(static::$cols['label'], static::$cols);
    }

    private static function makeAlias($name) {
        return str_replace('.','_',$name);
    }

    /**
     * @param $label
     * @param $name
     * @return Forms
     */
    public static function add($label, $name) {
        static::$cols = [];

        static::$cols['alias'] = static::makeAlias($name);
        static::$cols['name'] = $name;
        static::$cols['label'] = $label;

        static::putSingleton();
        return new static();
    }

    public function asText()
    {
        return new static();
    }

    public function validation($valid)
    {
        static::$cols['validation'] = $valid;
        static::putSingleton();
        return $this;
    }
}