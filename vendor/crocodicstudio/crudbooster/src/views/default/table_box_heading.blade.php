@if($button_bulk_action && ( ($button_delete && CRUDBooster::isDelete()) || $button_selected) )
    <div class="pull-{{ trans('crudbooster.left') }}">
        <div class="selected-action" style="display:inline-block;position:relative;">
            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class='fa fa-check-square-o'></i> {{trans("crudbooster.button_selected_action")}}
                <span class="fa fa-caret-down"></span></button>
            <ul class="dropdown-menu">
                @if($button_delete && CRUDBooster::isDelete())
                    <li><a href="javascript:void(0)" data-name='delete' title='{{trans('crudbooster.action_delete_selected')}}'><i
                                    class="fa fa-trash"></i> {{trans('crudbooster.action_delete_selected')}}</a></li>
                    @if(Request::segment(2)=='sales')
                    <li><a href="javascript:void(0)" data-name='approve_sales_in_house' title='Approve'>
                        <i class="fa fa-check"></i> Approve | Sales In House</a>
                    </li>
                    <li><a href="javascript:void(0)" data-name='approve_sales_agent' title='Approve'>
                        <i class="fa fa-check"></i> Approve | Sales Agent</a>
                    </li>
                    @endif
                @endif

                @if($button_selected)
                    @foreach($button_selected as $button)
                        <li><a href="javascript:void(0)"
                               data-name='{{$button["name"]}}'
                               title='{{$button["label"]}}'><i
                                        class="fa fa-{{$button['icon']}}"></i> {{$button['label']}}</a></li>
                    @endforeach
                @endif
            </ul><!--end-dropdown-menu-->
        </div><!--end-selected-action-->
    </div><!--end-pull-left-->
@else
    <div class="pull-{{ trans('crudbooster.left') }}">
        <h1 class="box-title" style="
        font-size: 20px;
        margin-top: 5px;
        ">Table Data</h1>
    </div>
@endif