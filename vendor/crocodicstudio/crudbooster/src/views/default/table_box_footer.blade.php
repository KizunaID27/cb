<div style="padding: 10px">
    <div class="row">
        <div class="col-md-8">{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</div>
        <?php
        $from = $result->count() ? ($result->perPage() * $result->currentPage() - $result->perPage() + 1) : 0;
        $to = $result->perPage() * $result->currentPage() - $result->perPage() + $result->count();
        $total = $result->total();
        ?>
        <div class="col-md-4" style="text-align: right">
            {{ trans("crudbooster.filter_rows_total") }}
            : {{ $from }} {{ trans("crudbooster.filter_rows_to") }} {{ $to }} {{ trans("crudbooster.filter_rows_of") }} {{ $total }}
        </div>
    </div>
</div>