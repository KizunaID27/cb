@extends('crudbooster::admin_template')

@section('content')

    @if(!is_null($pre_index_html) && !empty($pre_index_html))
        {!! $pre_index_html !!}
    @endif

    @if(request('return_url'))
        <p>
            <a href="{{ filter_var(request('return_url'), FILTER_VALIDATE_URL) }}">
                <i class='fa fa-chevron-circle-{{ trans('crudbooster.left') }}'></i>
                  <?php $label = strip_tags(request('label')); ?>
                &nbsp; {{ trans('crudbooster.form_back_to_list',['module'=>urldecode($label)]) }}
            </a>
        </p>
    @endif

    <div class="box">
        <div class="box-header">
            @include('crudbooster::default.table_box_heading')
            <div class="box-tools pull-{{ trans('crudbooster.right') }}" style="position: relative;margin-top: -5px;margin-right: -10px">
                @include('crudbooster::default.table_box_tools')
            </div>
            <br style="clear:both"/>
        </div>
        <div class="box-body table-responsive">
            @include("crudbooster::default.table")
        </div>

        <div class="box-footer">
            @include("crudbooster::default.table_box_footer")
        </div>
    </div>

    @if(!is_null($post_index_html) && !empty($post_index_html))
        {!! $post_index_html !!}
    @endif

    <!-- if form less -->
    @push('bottom')
        @include('crudbooster::default.partials.auto_modal_script')
    @endpush

@endsection
