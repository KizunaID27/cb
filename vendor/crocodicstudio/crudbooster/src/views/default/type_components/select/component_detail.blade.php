<?php

if ($form['datatable']) {
    $datatable = explode(',', $form['datatable']);
    $datatableCount = count($datatable);
    if($datatableCount==4) {
        $tableData = $datatable[0];
        $tableDataPK = CB::findPrimaryKey($tableData);
        $tableDataForeign = $datatable[1];
        $relationTable = $datatable[2];
        $relationTablePK = CB::findPrimaryKey($relationTable);
        $displayField = $datatable[3];
        $data = DB::table($tableData)
            ->join($relationTable, $relationTable.'.'.$relationTablePK, '=', $tableData.'.'.$tableDataForeign);
        foreach(\crocodicstudio\crudbooster\helpers\CB::getTableColumns($tableData) as $col) {
            $data->addselect($tableData.'.'.$col);
            $data->addselect($tableData.'.'.$col.' as '.$tableData.'_'.$col);
        }
        foreach(\crocodicstudio\crudbooster\helpers\CB::getTableColumns($relationTable) as $col) {
            $data->addselect($relationTable.'.'.$col.' as '.$relationTable.'_'.$col);
        }

        if(isset($form['datatable_value'])) {
            $data->where($form['datatable_value'], $value);
        }else{
            $data->where($tableData.'.'.$tableDataPK, $value);
        }

        $data = $data->first();
    }else{
        $tableData = $datatable[0];
        $tableDataPK = CB::findPrimaryKey($tableData);
        $displayField = $datatable[1];
        $data = DB::table($tableData);
        foreach(\crocodicstudio\crudbooster\helpers\CB::getTableColumns($tableData) as $col) {
            $data->addselect($tableData.'.'.$col);
            $data->addselect($tableData.'.'.$col.' as '.$tableData.'_'.$col);
        }

        if(isset($form['datatable_value'])) {
            $data->where($form['datatable_value'], $value);
        }else{
            $data->where($tableData.'.'.$tableDataPK, $value);
        }

        $data = $data->first();
    }


    if(isset($form['datatable_format'])) {
        $display = $form['datatable_format'];
    }else{
        $display = '['.$displayField.']';
    }

    foreach($data as $a=>$b) {
        $display = str_replace('['.$a.']',$b, $display);
    }

    echo $display;

}elseif ($form['dataenum']) {
    echo $value;
}
?>