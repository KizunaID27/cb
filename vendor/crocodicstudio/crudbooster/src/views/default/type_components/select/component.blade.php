<?php $default = ! empty($form['default']) ? $form['default'] : trans('crudbooster.text_prefix_option')." ".$form['label'];?>
@if($form['parent_select'])
    <?php
    $parent_select = (count(explode(",", $form['parent_select'])) > 1) ? explode(",", $form['parent_select']) : $form['parent_select'];
    $parent = is_array($parent_select) ? $parent_select[0] : $parent_select;
    $add_field = is_array($parent_select) ? $parent_select[1] : '';
    ?>
    @if(request()->ajax())
        @include('crudbooster::default.type_components.select.script_parent')
    @else
        @push('bottom')
            @include('crudbooster::default.type_components.select.script_parent')
        @endpush
    @endif

@endif
<div class='form-group {{$header_group_class}} {{ ($errors->first($name))?"has-error":"" }}' id='form-group-{{$name}}' style="{{@$form['style']}}">
    <label class='control-label col-sm-{{$form_label_width }}'>{{$form['label']}}
        @if($required)
            <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>
    <div class="{{$col_width?:'col-sm-'.$form_input_width}}">
        <select class='form-control' id="{{$name}}" data-value='{{$value}}' {{$required}} {!!$placeholder!!} {{$readonly}} {{$disabled}} name="{{$name}}">
            <option value=''>{{$default}}</option>
            <?php
            if (! $form['parent_select']) {
                if (@$form['dataquery']):

                    $query = DB::select(DB::raw($form['dataquery']));
                    if ($query) {
                        foreach ($query as $q) {
                            $selected = ($value == $q->value) ? "selected" : "";
                            echo "<option $selected value='$q->value'>$q->label</option>";
                        }
                    }

                endif;

                if (@$form['dataenum']):
                    $dataenum = $form['dataenum'];
                    $dataenum = (is_array($dataenum)) ? $dataenum : explode(";", $dataenum);

                    foreach ($dataenum as $d) {

                        $val = $lab = '';
                        if (strpos($d, '|') !== FALSE) {
                            $draw = explode("|", $d);
                            $val = $draw[0];
                            $lab = $draw[1];
                        } else {
                            $val = $lab = $d;
                        }

                        $select = ($value == $val) ? "selected" : "";

                        echo "<option $select value='$val'>$lab</option>";
                    }
                endif;

                if (@$form['datatable']):
                    $raw = explode(",", $form['datatable']);
                    $datatableCount = count($raw);

                    $format = $form['datatable_format'];

                    $selectTable = $raw[0];
                    $selectField = $raw[1];
                    $selectPK = CRUDBooster::findPrimaryKey($selectTable);

                    $selects_data = DB::table($selectTable);

                    foreach(\crocodicstudio\crudbooster\helpers\CRUDBooster::getTableColumns($selectTable) as $tab) {
                        $selects_data->addselect($selectTable.'.'.$tab.' as '.$tab);
                        $selects_data->addselect($selectTable.'.'.$tab.' as '.$selectTable.'_'.$tab);
                    }

                    if ($datatableCount == 4) {
                        $relationTable = $raw[2];
                        $relationPK = CRUDBooster::findPrimaryKey($relationTable);
                        $relationDisplayField = $raw[3];
                        $selects_data->join($relationTable, $relationTable.'.'.$relationPK,'=', $selectTable.'.'.$raw[1]);
                        foreach(\crocodicstudio\crudbooster\helpers\CRUDBooster::getTableColumns($relationTable) as $tab) {
                            $selects_data->addselect($relationTable.'.'.$tab.' as '.$relationTable.'_'.$tab);
                        }
                    }

                    if (\Schema::hasColumn($selectTable, 'deleted_at')) {
                        $selects_data->whereNull($selectTable.'.deleted_at');
                    }

                    if (@$form['datatable_where']) {
                        $selects_data->whereraw($form['datatable_where']);
                    }

                    $selects_data = $selects_data->get();

                    $resultFinal = [];
                    foreach($selects_data as $data)
                    {
                        $display = "";
                        if($datatableCount == 4) {
                            $display = $data->{$raw[3]};
                        }else{
                            $display = $data->{$raw[1]};
                        }

                        if(isset($form['datatable_format'])) {
                            $display = $form['datatable_format'];
                            foreach($data as $a=>$b) {
                                $display = str_replace("[".$a."]",$b,$display);
                            }
                        }

                        $dataID = $data->$selectPK;
                        if(isset($form['datatable_value'])) {
                            $dataID = $data->{$form['datatable_value']};
                        }

                        $resultFinal[] = ['id'=>$dataID, 'display'=>$display];
                    }

                    foreach ($resultFinal as $d) {
                        $select = ($value == $d['id']) ? "selected" : "";
                        echo "<option $select value='".$d['id']."'>".$d['display']."</option>";
                    }
                endif;
            } //end if not parent select
            ?>
        </select>
        <div class="text-danger">{!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}</div>
        <p class='help-block'>{{ @$form['help'] }}</p>
    </div>
</div>