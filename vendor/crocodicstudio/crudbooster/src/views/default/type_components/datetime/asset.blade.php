<script>
    $(function () {
        if($(".datetimepicker").length > 0) {
            $(".datetimepicker").daterangepicker({
                minDate: '1900-01-01',
                singleDatePicker: true,
                showDropdowns: true,
                timePicker:true,
                timePicker12Hour: false,
                timePickerIncrement: 5,
                timePickerSeconds: true,
                autoApply: true,
                format:'YYYY-MM-DD HH:mm:ss'
            })
        }
    })
</script>