<tr class="active">

    <?php if($button_bulk_action && !g('fileformat')):?>
    <th width='3%'><input type='checkbox' id='checkall'/></th>
    <?php endif;?>

    <?php if($show_numbering):?>
    <th width="1%">{{ trans('crudbooster.no') }}</th>
    <?php endif;?>
    <?php foreach ($columns as $col) {
        if (isset($col['visible']) && $col['visible'] === FALSE) continue;

        $label = $col['label'];
        $name = $col['name'];
        $width = ($col['width']) ?: "auto";
        echo "<th style='font-weight:bold' width='$width'>";
        if (request('orderby') == $col['alias']) {
            switch (request('orderby_val')) {
                case 'asc':
                    $url = request()->fullUrlWithQuery(['orderby'=>$col['alias'],'orderby_val'=>'desc']);
                    if(!g('fileformat')) {
                        echo "<a href='$url' title='Click to sort descending'>";
                    }

                    echo "$label";

                    if(!g('fileformat')) {
                        echo " &nbsp; <i class='fa fa-sort-desc'></i></a>";
                    }

                    break;
                default:
                case 'desc':
                    $url = request()->fullUrlWithQuery(['orderby'=>$col['alias'],'orderby_val'=>'asc']);
                    if(!g('fileformat')) {
                        echo "<a href='$url' title='Click to sort ascending'>";
                    }
                    echo "$label";

                    if(!g('fileformat')) {
                        echo " &nbsp; <i class='fa fa-sort-asc'></i></a>";
                    }
                    break;
            }
        } else {
            $url = request()->fullUrlWithQuery(['orderby'=>$col['alias'],'orderby_val'=>'asc']);
            if(!g('fileformat')) {
                echo "<a href='$url' title='Click to sort ascending'>";
            }
            echo "$label";

            if(!g('fileformat')) {
                echo " &nbsp; <i class='fa fa-sort'></i></a>";
            }
        }

        echo "</th>";
    }
    ?>

    @if($button_table_action && !g('fileformat'))
        @if(CRUDBooster::isUpdate() || CRUDBooster::isDelete() || CRUDBooster::isRead())
            <th width='{{$button_action_width?:"auto"}}' style="text-align:center">{{trans("crudbooster.action_label")}}</th>
        @endif
    @endif
</tr>