
    </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('crudbooster::footer')

    </div><!-- ./wrapper -->


    @include('crudbooster::admin_template_plugins')
    @yield('custom-scripts')
    <!-- load js -->
    @if($load_js)
        @foreach($load_js as $js)
            <script src="{{$js}}"></script>
        @endforeach
    @endif
    <script type="text/javascript">
        var site_url = "{{url('/')}}";
        @if($script_js)
            {!! $script_js !!}
        @endif
    </script>

    @stack('bottom')

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
              Both of these plugins are recommended to enhance the
              user experience -->
    </body>
    </html>