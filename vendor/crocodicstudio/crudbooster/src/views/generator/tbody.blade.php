
@foreach($result as $data)
    <tr>
    @foreach($columns as $column)
        <td>
            @if($column['image']===true)
                @if($data->{$column['name']})
                    <a href="{{ asset($data->{ $column['name'] }) }}" data-lightbox="roadtrip">
                        <img width="35px" height="35px" class="img-rounded" src="{{ asset($data->{ $column['name'] }) }}" alt="Thumbnail">
                    </a>
                @else
                    <img class="img-rounded" src="{{ dummyBlankImage() }}" alt="No Image Found" width="35px" height="35px">
                @endif
            @else
                {{ $data->{ $column['name'] } }}
            @endif
        </td>
    @endforeach
        <td>
            <a href="javascript:;" onclick="edit{{$uniqueID}}( {{ $data->$primaryKey }} )" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
            <a href="javascript:;" onclick="remove{{$uniqueID}}( {{ $data->$primaryKey }} )" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
        </td>
    </tr>
@endforeach