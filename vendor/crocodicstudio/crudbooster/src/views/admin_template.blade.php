@if(!request()->ajax())
    @include('crudbooster::admin_template_before')
@endif
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- Your Page Content Here -->
    @yield('content')

     <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Detail Location <span id="desc"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 modal_body_map">
                            <div class="location-map" id="location-map">
                                <div style="width: 600px; height: 400px;" id="map_canvas"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(Request::segment(2)=='checkin_checkout')
    	<script>
			$(document).ready(function(){
			  var map = null;
			  var myMarker;
			  var myLatlng;

			  function initializeGMap(lat, lng){
			    myLatlng = new google.maps.LatLng(lat, lng);

			    var myOptions = {
			      zoom: 14,
			      zoomControl: true,
			      center: myLatlng,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };

			    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

			    var image ={
			        url: "{{asset('image/marker_green.png')}}", // url
			        scaledSize: new google.maps.Size(30, 45), // scaled size
			    };

			    myMarker = new google.maps.Marker({
			      position: myLatlng,
			      icon: image,
			    });
			    myMarker.setMap(map);
			  }

			  // Re-init map before show modal
			  $('#modal').on('show.bs.modal', function(event){
			    var button = $(event.relatedTarget);
			    initializeGMap(button.data('lat'), button.data('lng'));

			    $("#location-map").css("width", "100%");
			    $("#map_canvas").css("width", "100%");
			   	$("#desc").html( button.data('desc') );
			  });

			  // Trigger map resize event after modal shown
			  $('#modal').on('shown.bs.modal', function(){
			    google.maps.event.trigger(map, "resize");
			    map.setCenter(myLatlng);
			  });
			});

    	</script>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2tvK3kd4MNMTiAEBUEl788PVqYTAopTE"></script>
    @endif

    <script type="text/javascript">
    	$('input[type="checkbox"][name="menu[]"]').bind('click',function(){
	    	if($(this).is(':checked')){
	        }else{
	        	var isi = $(this).val();
				var arr = isi.split('/');

	        	$.ajax({
		        	type: "GET",
		            url: "{{url('admin/suppliers/delete-uncheck')}}?",
		            cache: false,
		          	data:  {'id_menu': arr[0], 'id_supplier': arr[1] },
		            success: function(result){ 
		            }
		        });
	        }
	 	});
    </script>

    <!-- custom ivan -->
    @if(Request::segment(2)=='employees' OR Request::segment(2)=='resign_employee')
		<script>
			$("select[name='type']").change(function(){
				var val = $("#type").val();
				if(val=='Permanent'){
					$("#leave_quota").show(100); 
					$("#leave_quota").prop('required',true);
				}else{
					$("#leave_quota").hide(100);
					$("#leave_quota").prop('required',false);
				}
			});    	
		</script>
	@endif

	@if(Request::segment(2)=='leave_type')
		<script>
			$("select[name='cut_leave_quota']").change(function(){
				var val = $("select[name='cut_leave_quota']").val();
				if(val=='No'){
					$("#additional_leave_quota").show(100); 
					$("#help-block").show(100);
					$("#additional_leave_quota").prop('required',true);
				}else{
					$("#additional_leave_quota").hide(100);
					$("#help-block").hide(100);
					$("#additional_leave_quota").prop('required',false);
				}
			});    	
		</script>
	@endif

@if(!request()->ajax())
    @include('crudbooster::admin_template_after')
@endif