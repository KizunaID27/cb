<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Apis\SessionID;

class SIDMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        $session = new SessionID();

        if(config('app.api_file_log')) {
            \Log::debug("Session: ".print_r($session->get(), true));
            \Log::debug("API HIT : ".$request->fullUrl());
            \Log::debug("API Params : ".print_r($request->all(), true));
        }

        if(!$request->header("sid") || $session->guard()==false) {
            return response()->json(['api_status'=>0,'api_message'=>'SESSION_EXPIRED']);
        }

        return $next($request);
    }

}
