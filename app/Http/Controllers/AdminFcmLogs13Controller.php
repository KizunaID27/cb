<?php 
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminFcmLogs13Controller extends CBController {

    public function cbInit() {
        
        $this->table 			   = "fcm_logs";	        
        $this->title_field         = "title";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		    = "normal"; //normal,mini,collapse,collapse-mini							      

		Col::add("Title","title");
		Col::add("Content","content");
		Col::add("Fcm Data","fcm_data");
		Col::add("Fcm Response","fcm_response");
		Col::add("Fcm Tokens","fcm_tokens");
		Col::add("Fcm Key","fcm_key");

		$this->form = [];
		$this->form[] = ["label"=>"Title","name"=>"title","type"=>"text","validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
		$this->form[] = ["label"=>"Content","name"=>"content","type"=>"text","validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
		$this->form[] = ["label"=>"Fcm Data","name"=>"fcm_data","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Fcm Response","name"=>"fcm_response","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Fcm Tokens","name"=>"fcm_tokens","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Fcm Key","name"=>"fcm_key","type"=>"text","validation"=>"required|min:1|max:255"];

    }
}