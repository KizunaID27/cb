<?php 
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminApiLogs13Controller extends CBController {

    public function cbInit() {
        
        $this->table 			   = "api_logs";	        
        $this->title_field         = "name";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		    = "normal"; //normal,mini,collapse,collapse-mini							      

		Col::add("Name","name");
		Col::add("Url","url");
		Col::add("Useragent","useragent");
		Col::add("Sdata","sid_data");
		Col::add("Request Header","request_header");
		Col::add("Request Data","request_data");

		$this->form = [];
		$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
		$this->form[] = ["label"=>"Url","name"=>"url","type"=>"text","validation"=>"required|url","placeholder"=>"Please enter a valid URL"];
		$this->form[] = ["label"=>"Useragent","name"=>"useragent","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Sdata","name"=>"sid_data","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Request Header","name"=>"request_header","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Request Data","name"=>"request_data","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Response Data","name"=>"response_data","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Duration","name"=>"duration","type"=>"money","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Ip","name"=>"ip","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Request Method","name"=>"request_method","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Status","name"=>"status","type"=>"number","validation"=>"required|integer|min:0"];

    }
}