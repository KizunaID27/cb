<?php 
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminCmsUsers1Controller extends CBController {

    public function cbInit() {
        
        $this->table 			   = "cms_users";	        
        $this->title_field         = "name";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		    = "normal"; //normal,mini,collapse,collapse-mini							      

		Col::add("Privileges","name")->fetchFrom("cms_privileges","id_cms_privileges");
		Col::add("Name","name");
		Col::add("Photo","photo");
		Col::add("Phone","phone");
		Col::add("Address","address");

		$this->form = [];
		$this->form[] = ["label"=>"Cms Privileges","name"=>"id_cms_privileges","type"=>"select2","validation"=>"required|integer|min:0","datatable"=>"cms_privileges,name"];
		$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
		$this->form[] = ["label"=>"Photo","name"=>"photo","type"=>"upload","validation"=>"required|image|max:3000","help"=>"File types support : JPG, JPEG, PNG, GIF, BMP"];
		$this->form[] = ["label"=>"Phone","name"=>"phone","type"=>"number","validation"=>"required|numeric","placeholder"=>"You can only enter the number only"];
		$this->form[] = ["label"=>"Address","name"=>"address","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","validation"=>"required|min:1|max:255|email|unique:cms_users","placeholder"=>"Please enter a valid email address"];
		$this->form[] = ["label"=>"Password","name"=>"password","type"=>"password","validation"=>"min:3|max:32","help"=>"Minimum 5 characters. Please leave empty if you did not change the password."];
		$this->form[] = ["label"=>"Nik","name"=>"nik","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Date Of Birth","name"=>"date_of_birth","type"=>"date","validation"=>"required|date"];
		$this->form[] = ["label"=>"No Npwp","name"=>"no_npwp","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Photo Ktp","name"=>"photo_ktp","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Master Status Id","name"=>"master_status_id","type"=>"select2","validation"=>"required|integer|min:0","datatable"=>"master_status,id"];
		$this->form[] = ["label"=>"Switch Status","name"=>"switch_status","type"=>"number","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Nik Ktp","name"=>"nik_ktp","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Mother Name","name"=>"mother_name","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Religion","name"=>"religion","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Child","name"=>"child","type"=>"number","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Gender","name"=>"gender","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Leave Quota","name"=>"leave_quota","type"=>"number","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Remaining Days Off","name"=>"remaining_days_off","type"=>"number","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Work Start","name"=>"work_start","type"=>"date","validation"=>"required|date"];
		$this->form[] = ["label"=>"End Of Probation","name"=>"end_of_probation","type"=>"date","validation"=>"required|date"];
		$this->form[] = ["label"=>"Master Education Id","name"=>"master_education_id","type"=>"select2","validation"=>"required|integer|min:0","datatable"=>"master_education,id"];
		$this->form[] = ["label"=>"No Ijazah","name"=>"no_ijazah","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Bpjs","name"=>"bpjs","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Expired","name"=>"expired","type"=>"date","validation"=>"required|date"];
		$this->form[] = ["label"=>"Position","name"=>"position","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Remark","name"=>"remark","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Account Number","name"=>"account_number","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Banks Id","name"=>"banks_id","type"=>"select2","validation"=>"required|integer|min:0","datatable"=>"banks,id"];
		$this->form[] = ["label"=>"Salary","name"=>"salary","type"=>"money","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Latitude","name"=>"latitude","type"=>"hidden","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Longitude","name"=>"longitude","type"=>"hidden","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Last Checkin","name"=>"last_checkin","type"=>"datetime","validation"=>"required|date_format:Y-m-d H:i:s"];
		$this->form[] = ["label"=>"Parent Cms Users","name"=>"parent_cms_users","type"=>"number","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Is Active","name"=>"is_active","type"=>"radio","validation"=>"required|integer","dataenum"=>"Array"];
		$this->form[] = ["label"=>"Regid","name"=>"regid","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Gross Salary","name"=>"gross_salary","type"=>"money","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Basic Salary","name"=>"basic_salary","type"=>"money","validation"=>"required|integer|min:0"];
		$this->form[] = ["label"=>"Type","name"=>"type","type"=>"text","validation"=>"required|min:1|max:255"];

    }
}