<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/16/2018
 * Time: 8:22 PM
 */

namespace App\Http\Controllers\Apis;


use App\Repos\Mutations;
use App\Repos\Outlets;
use App\Repos\Suppliers;

class SessionModel
{

    public $id;
    public $name;
    public $outlet_id;
    public $outlet_name;
    public $suppliers_id;
    public $suppliers_name;
    public $mutations_id;
    public $mutations_code;
    public $cms_privileges_id;
    public $cms_privileges_name;
    public $ip;
    public $useragent;

    public function getUserId()
    {
        return $this->getId();
    }

    public function getUserName()
    {
        return $this->getName();
    }

    /**
     * @return \App\Repos\CmsUsers
     */
    public function getUser()
    {
        return $this->getMutation()->getCmsUsers();
    }

    /**
     * @return Outlets
     */
    public function getOutlet()
    {
        return Outlets::findById($this->outlet_id);
    }

    /**
     * @return Suppliers
     */
    public function getSupplier()
    {
        return Suppliers::findById($this->suppliers_id);
    }

    /**
     * @return Mutations
     */
    public function getMutation()
    {
        return Mutations::findById($this->mutations_id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SessionModel
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return SessionModel
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutletId()
    {
        return $this->outlet_id;
    }

    /**
     * @param mixed $outlet_id
     * @return SessionModel
     */
    public function setOutletId($outlet_id)
    {
        $this->outlet_id = $outlet_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutletName()
    {
        return $this->outlet_name;
    }

    /**
     * @param mixed $outlet_name
     * @return SessionModel
     */
    public function setOutletName($outlet_name)
    {
        $this->outlet_name = $outlet_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuppliersId()
    {
        return $this->suppliers_id;
    }

    /**
     * @param mixed $suppliers_id
     * @return SessionModel
     */
    public function setSuppliersId($suppliers_id)
    {
        $this->suppliers_id = $suppliers_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuppliersName()
    {
        return $this->suppliers_name;
    }

    /**
     * @param mixed $suppliers_name
     * @return SessionModel
     */
    public function setSuppliersName($suppliers_name)
    {
        $this->suppliers_name = $suppliers_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMutationsId()
    {
        return $this->mutations_id;
    }

    /**
     * @param mixed $mutations_id
     * @return SessionModel
     */
    public function setMutationsId($mutations_id)
    {
        $this->mutations_id = $mutations_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMutationsCode()
    {
        return $this->mutations_code;
    }

    /**
     * @param mixed $mutations_code
     * @return SessionModel
     */
    public function setMutationsCode($mutations_code)
    {
        $this->mutations_code = $mutations_code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCmsPrivilegesId()
    {
        return $this->cms_privileges_id;
    }

    /**
     * @param mixed $cms_privileges_id
     * @return SessionModel
     */
    public function setCmsPrivilegesId($cms_privileges_id)
    {
        $this->cms_privileges_id = $cms_privileges_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCmsPrivilegesName()
    {
        return $this->cms_privileges_name;
    }

    /**
     * @param mixed $cms_privileges_name
     * @return SessionModel
     */
    public function setCmsPrivilegesName($cms_privileges_name)
    {
        $this->cms_privileges_name = $cms_privileges_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     * @return SessionModel
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUseragent()
    {
        return $this->useragent;
    }

    /**
     * @param mixed $useragent
     * @return SessionModel
     */
    public function setUseragent($useragent)
    {
        $this->useragent = $useragent;
        return $this;
    }



}