$( document ).ready(function() {
	var datava1 = $('#id_project_unit').attr('data-value');
	var datava2 = $('#id_project').attr('data-value');
	$.ajax({
		url : site_url+'/admin/leads/changeproject',
		type : "get",
		data : {
			id_project : datava2,
			id_project_unit : datava1
		},
		success: function(result) {
			$('#id_project_unit').html(result);
		}
	})

	$('#id_project').on('change', function() {
		var val = $('#id_project').val();
		$.ajax({
			url : site_url+'/admin/leads/changeproject',
			type : "get",
			data : {
				id_project : val
			},
			beforeSend: function() {
				$('#id_project_unit').html("<option value=''>Please wait loading... Project Unit");
			},
			success: function(result) {
				$('#id_project_unit').html(result);
			}
		})
	})
});