$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();


    function cb(start, end) {
        $('#tanggalrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#startdate1').val(start.format('YYYY-MM-DD'));
        $('#endate1').val(end.format('YYYY-MM-DD'));
    }

    $('#tanggalrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
       }
   }, cb);

    cb(start, end);

    var start2 = moment().subtract(29, 'days');
    var end2 = moment();


    function cb2(start2, end2) {
        $('#tanggalrange2 span').html(start2.format('MMMM D, YYYY') + ' - ' + end2.format('MMMM D, YYYY'));
        $('#startdate2').val(start2.format('YYYY-MM-DD'));
        $('#endate2').val(end2.format('YYYY-MM-DD'));
    }

    $('#tanggalrange2').daterangepicker({
        startDate: start2,
        endDate: end2,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
       }
   }, cb2);

    cb2(start2, end2);


});